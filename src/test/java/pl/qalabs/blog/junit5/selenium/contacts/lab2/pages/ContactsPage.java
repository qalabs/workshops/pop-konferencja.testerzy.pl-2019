package pl.qalabs.blog.junit5.selenium.contacts.lab2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ContactsPage extends ApplicationPage {

    private final By contactsPageLocator = By.id("contacts");
    private WebElement contactsPage;

    public ContactsPage(RemoteWebDriver driver) {
        super(driver);
        this.contactsPage = driver.findElement(contactsPageLocator);
    }

    @Override
    protected boolean isAt() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(contactsPageLocator));
        return true;
    }

    public ContactsList search(String search) {
        return new ContactsList(driver);
    }

    public ContactsList contactsList() {
        return new ContactsList(driver);
    }

    public ContactFormDialog contactFormDialog() {
        return new ContactFormDialog(driver);
    }
}
