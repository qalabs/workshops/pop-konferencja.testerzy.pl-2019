package pl.qalabs.blog.junit5.selenium.contacts.lab2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Snackbar extends ApplicationPage {

    private final By snackbarLocator = By.id("snackbar");
    private WebElement snackbar;

    Snackbar(RemoteWebDriver driver) {
        super(driver);
        this.snackbar = driver.findElement(snackbarLocator);
    }

    @Override
    protected boolean isAt() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(snackbarLocator));
        return false;
    }

    public String getText() {
        return null;
    }

    public void close() {

    }
}
