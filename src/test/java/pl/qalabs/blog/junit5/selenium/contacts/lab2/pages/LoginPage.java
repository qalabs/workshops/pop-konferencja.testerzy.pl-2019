package pl.qalabs.blog.junit5.selenium.contacts.lab2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class LoginPage extends ApplicationPage {

    private final By loginPageLocator = By.id("login-form");
    private WebElement loginPage;

    public static ContactsPage loginDefault(RemoteWebDriver driver) {
        return new LoginPage(driver).loginAs("contacts", "demo");
    }

    LoginPage(RemoteWebDriver driver) {
        super(driver);
        this.loginPage = driver.findElement(loginPageLocator);
    }

    @Override
    public boolean isAt() {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(loginPageLocator)));
        return true;
    }

    public ContactsPage loginAs(String username, String password) {
        return new ContactsPage(driver);
    }

    public LoginPage loginAsExpectingValidationErrors(String username, String password) {
        return this;
    }

    public LoginPage loginAsExpectingError(String username, String password) {
        return this;
    }

    public List<String> getValidationErrors() {
        return List.of();
    }

    public String getLoginError() {
        return null;
    }
}
