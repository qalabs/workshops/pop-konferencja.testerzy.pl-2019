package pl.qalabs.blog.junit5.selenium.contacts.lab2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class ContactsList extends ApplicationPage {

    private final By contactsListLocator = By.id("contacts-list");
    private WebElement contactsList;

    ContactsList(RemoteWebDriver driver) {
        super(driver);
        this.contactsList = driver.findElement(contactsListLocator);
    }

    @Override
    protected boolean isAt() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(contactsListLocator));
        return true;
    }

    public int count() {
        return 0;
    }

    public List<Contact> getContacts() {
        return List.of();
    }

    public Contact getContactByName(String name) {
        return null;
    }

    public ContactDetailsDialog contactDetailsDialog(String name) {
        return new ContactDetailsDialog(driver);
    }
}
