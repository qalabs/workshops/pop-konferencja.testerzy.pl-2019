package pl.qalabs.blog.junit5.selenium.contacts.lab1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class ContactsAppTests {

    private static final String BASE_URL = "https://qalabs.pl/demos/contacts";
    private RemoteWebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    void setUp() {

        // initialize driver with implicit wait and maximized window

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 1);

        // navigate to login page and verify login form is visible

        driver.get(BASE_URL);

        // login

        WebElement usernameField = driver.findElement(By.id("username"));
        usernameField.sendKeys("contacts");

        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("demo");

        WebElement loginButton = driver.findElement(By.cssSelector("#login-form > div.v-card__actions > button"));
        loginButton.click();

        // wait for logout button to be present

        wait.until(ExpectedConditions.textToBe(By.cssSelector("#app-nav button:nth-child(1) > div.v-btn__content"), "LOGOUT"));
    }


    @AfterEach
    void tearDown() {

        // quit the driver

        driver.quit();
    }

    @Test
    void createsBasicContact() {

        // open contact form dialog

        WebElement newContactButton = driver.findElement(By.cssSelector("#contacts .v-dialog__activator > button"));
        newContactButton.click();

        WebElement dialog = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));

        // create contact

        WebElement nameField = dialog.findElement(By.cssSelector("input[name=\"contact-name\"]"));
        nameField.sendKeys("Weronika Gubała");

        WebElement saveButton = dialog.findElement(By.cssSelector("div.v-card__actions > button:last-child"));
        saveButton.click();

        // verify snackbar

        Assertions.assertDoesNotThrow(() -> wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("snackbar"), "Contact created successfully.")));

        // get contacts list

        WebElement contactsList = driver.findElement(By.cssSelector("#contacts-list"));

        // verify contact on the list

        List<WebElement> namesElements = contactsList.findElements(By.cssSelector("span[data-property=contact-name]"));
        List<String> names = namesElements.stream().map(WebElement::getText).collect(Collectors.toList());

        Assertions.assertTrue(names.stream().anyMatch(name -> name.equals("Weronika Gubała")));

    }

    @Test
    void createsContact() {

        // open contact form dialog

        WebElement newContactButton = driver.findElement(By.cssSelector("#contacts .v-dialog__activator > button"));
        newContactButton.click();

        WebElement dialog = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("contact-form")));

        // create contact

        WebElement nameField = dialog.findElement(By.cssSelector("input[name=\"contact-name\"]"));
        nameField.sendKeys("Weronika Gubała");

        WebElement emailField = dialog.findElement(By.cssSelector("input[name=\"contact-email\"]"));
        emailField.sendKeys("weronika.gubala@workers.co");

        WebElement phoneField = dialog.findElement(By.cssSelector("input[name=\"contact-phone\"]"));
        phoneField.sendKeys("673 87 89 22");

        WebElement notesField = dialog.findElement(By.cssSelector("input[name=\"contact-notes\"]"));
        notesField.sendKeys("Weronika is simply awesome Scrum Master!");

        WebElement saveButton = dialog.findElement(By.cssSelector("div.v-card__actions > button:last-child"));
        saveButton.click();

        // verify snackbar

        Assertions.assertDoesNotThrow(() -> wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("snackbar"), "Contact created successfully.")));

        // get contacts list

        WebElement contactsList = driver.findElement(By.cssSelector("#contacts-list"));

        // verify contact on the list

        List<WebElement> namesElements = contactsList.findElements(By.cssSelector("span[data-property=contact-name]"));
        List<String> names = namesElements.stream().map(WebElement::getText).collect(Collectors.toList());

        Assertions.assertTrue(names.stream().anyMatch(name -> name.equals("Weronika Gubała")));

        List<WebElement> emailsElements = contactsList.findElements(By.cssSelector("span[data-property=contact-email]"));
        List<String> emails = emailsElements.stream().map(WebElement::getText).collect(Collectors.toList());

        Assertions.assertTrue(emails.stream().anyMatch(name -> name.equals("weronika.gubala@workers.co")));

        List<WebElement> phonesElements = contactsList.findElements(By.cssSelector("span[data-property=contact-phone]"));
        List<String> phones = phonesElements.stream().map(WebElement::getText).collect(Collectors.toList());

        Assertions.assertTrue(phones.stream().anyMatch(name -> name.equals("673 87 89 22")));

    }

    @Test
    void showsContactDetails() {

        // find contact on the list

        WebElement contactRow = driver.findElement(By.xpath("//*[@data-property=\"contact-name\"][contains(text(), \"Emeryk Kaczmarek\")]/../.."));

        // show contact details

        WebElement contactNameCell = contactRow.findElement(By.cssSelector("span[data-property=contact-name]"));
        contactNameCell.click();

        WebElement dialog = driver.findElement(By.id("contact-details"));
        dialog = wait.until(ExpectedConditions.visibilityOf(dialog));

        // verify contact details

        WebElement name = dialog.findElement(By.cssSelector("span[data-property=contact-name]"));
        Assertions.assertEquals("Emeryk Kaczmarek", name.getText());

        WebElement email = dialog.findElement(By.cssSelector("div[data-property=contact-email] .v-list__tile__title"));
        Assertions.assertEquals("emeryk.kaczmarek@rhyta.com", email.getText());

        WebElement phone = dialog.findElement(By.cssSelector("div[data-property=contact-phone] .v-list__tile__title"));
        Assertions.assertEquals("443 93 21 53", phone.getText());
    }

    @Test
    void modifiesContactName() {

        // find contact on the list

        WebElement contactRow = driver.findElement(By.xpath("//*[@data-property=\"contact-name\"][contains(text(), \"Julita Walczak\")]/../.."));

        // open contact form dialog

        WebElement contactEditButton = contactRow.findElement(By.cssSelector("td.justify-center.layout.px-0 > i:nth-child(2)"));
        contactEditButton.click();

        WebElement dialog = driver.findElement(By.id("contact-form"));
        wait.until(ExpectedConditions.visibilityOf(dialog));

        WebElement contactNameInput = dialog.findElement(By.cssSelector("input[name=contact-name]"));
        contactNameInput.sendKeys("owa");

        WebElement saveButton = dialog.findElement(By.cssSelector("div.v-card__actions > button:nth-child(4)"));
        saveButton.click();

        // verify snackbar

        Assertions.assertDoesNotThrow(() -> wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("snackbar"), "Contact modified successfully.")));

        // find contact on the list

        contactRow = Assertions.assertDoesNotThrow(() -> wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[@data-property=\"contact-name\"][contains(text(), \"Julita Walczakowa\")]/../..")))));

        // show contact details

        WebElement contactNameCell = contactRow.findElement(By.cssSelector("span[data-property=contact-name]"));
        contactNameCell.click();

        dialog = driver.findElement(By.id("contact-details"));
        dialog = wait.until(ExpectedConditions.visibilityOf(dialog));

        // verify contact details

        WebElement name = dialog.findElement(By.cssSelector("span[data-property=contact-name"));
        Assertions.assertEquals("Julita Walczakowa", name.getText());

    }

    @Test
    void removesContact() {

        // find contact on the list

        WebElement contactRow = driver.findElement(By.xpath("//*[@data-property=\"contact-name\"][contains(text(), \"Julita Walczak\")]/../.."));

        // delete selected contact

        WebElement deleteContactButton = contactRow.findElement(By.cssSelector("td.justify-center.layout.px-0 > i:nth-child(3)"));
        deleteContactButton.click();

        Alert confirmation = wait.until(ExpectedConditions.alertIsPresent());
        confirmation.accept();

        // verify snackbar

        Assertions.assertDoesNotThrow(() -> wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("snackbar"), "Contact removed successfully.")));

        // get contacts list

        WebElement contactsList = driver.findElement(By.cssSelector("#contacts-list"));

        // verify contact on the list

        List<WebElement> namesElements = contactsList.findElements(By.cssSelector("#contacts-list span[data-property=contact-name]"));
        List<String> names = namesElements.stream().map(WebElement::getText).collect(Collectors.toList());

        Assertions.assertFalse(names.stream().anyMatch(name -> name.equals("Julita Walczak")));
    }

    @Test
    void searchesByName() {

        // search for contact

        WebElement searchInput = driver.findElement(By.cssSelector("#contacts input[type=\"text\"]"));
        searchInput.sendKeys("fryderyk");

        // get contacts list

        WebElement contactsList = driver.findElement(By.cssSelector("#contacts-list"));

        // verify contact on the list

        List<WebElement> namesElements = contactsList.findElements(By.cssSelector("span[data-property=contact-name]"));
        List<String> names = namesElements.stream().map(WebElement::getText).collect(Collectors.toList());

        Assertions.assertTrue(names.stream().anyMatch(name -> name.equals("Fryderyk Maciejewski")));

        // show contact details

        WebElement contactRow = driver.findElement(By.xpath("//*[@data-property=\"contact-name\"][contains(text(), \"Fryderyk Maciejewski\")]/../.."));

        WebElement contactNameCell = contactRow.findElement(By.cssSelector("span[data-property=contact-name]"));
        contactNameCell.click();

        WebElement dialog = driver.findElement(By.id("contact-details"));
        dialog = wait.until(ExpectedConditions.visibilityOf(dialog));

        // verify contact details

        WebElement name = dialog.findElement(By.cssSelector("span[data-property=contact-name]"));
        Assertions.assertEquals("Fryderyk Maciejewski", name.getText());

        WebElement email = dialog.findElement(By.cssSelector("div[data-property=contact-email] .v-list__tile__title"));
        Assertions.assertEquals("fryderyk.maciejewski@teleworm.us", email.getText());

        WebElement phone = dialog.findElement(By.cssSelector("div[data-property=contact-phone] .v-list__tile__title"));
        Assertions.assertEquals("195 24 07 22", phone.getText());
    }

    @Test
    void searchesByLabel() {

        // search for contact

        WebElement searchInput = driver.findElement(By.cssSelector("#contacts input[type=\"text\"]"));
        searchInput.sendKeys("co-workers");

        // get contacts list

        WebElement contactsList = driver.findElement(By.cssSelector("#contacts-list"));

        // verify contact on the list

        List<WebElement> namesElements = contactsList.findElements(By.cssSelector("span[data-property=contact-name]"));
        List<String> names = namesElements.stream().map(WebElement::getText).collect(Collectors.toList());

        Assertions.assertLinesMatch(List.of("Emeryk Kaczmarek", "Julita Walczak"), names);
    }
}
