package pl.qalabs.blog.junit5.selenium.contacts.lab2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class ContactFormDialog extends ApplicationPage {

    private final By contactFormDialogLocator = By.id("contact-form");
    private WebElement contactForm;

    ContactFormDialog(RemoteWebDriver driver) {
        super(driver);
        this.contactForm = driver.findElement(contactFormDialogLocator);
    }

    @Override
    protected boolean isAt() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(contactFormDialogLocator));
        return true;
    }

    public ContactsPage save(Contact contact) {
        return new ContactsPage(driver);
    }

    public ContactsPage saveExpectingValidationErrors(Contact contact) {
        return new ContactsPage(driver);
    }

    public ContactsPage cancel() {
        return new ContactsPage(driver);
    }

    public List<String> getValidationErrors() {
        return List.of();
    }
}
