package pl.qalabs.blog.junit5.selenium.contacts.lab1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class LoginLogoutTests {

    private static final String BASE_URL = "https://qalabs.pl/demos/contacts";
    private RemoteWebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    void setUp() {

        // initialize driver with implicit wait and maximized window

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 1);

        // navigate to login page and verify login form is visible

        driver.get(BASE_URL + "#/login");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login-form")));
    }

    @AfterEach
    void tearDown() {

        // quit the driver

        driver.quit();
    }

    @Test
    void logsIn() {

        // fill in the form

        WebElement usernameField = driver.findElement(By.id("username"));
        usernameField.sendKeys("contacts");

        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("demo");

        // login

        WebElement loginButton = driver.findElement(By.cssSelector("#login-form > div.v-card__actions > button"));
        loginButton.click();

        // wait for logout button to be present

        Assertions.assertDoesNotThrow(() -> wait.until(ExpectedConditions.textToBe(By.cssSelector("#app-nav button:nth-child(1) > div.v-btn__content"), "LOGOUT")));

        // logout

        WebElement logoutButton = driver.findElement(By.cssSelector("#app-nav button:nth-child(1) > div.v-btn__content"));
        logoutButton.click();

        // verify login form is visible

        Assertions.assertDoesNotThrow(() -> wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login-form"))));
    }

    @Test
    void logsInExpectingValidationErrors() {

        // login without filling in the form

        WebElement loginButton = driver.findElement(By.cssSelector("#login-form > div.v-card__actions > button"));
        loginButton.click();

        // get validation errors

        List<WebElement> elements = driver.findElements(By.cssSelector("#login-form .v-text-field__details .error--text"));
        wait.until(ExpectedConditions.visibilityOfAllElements(elements));

        // assert validation errors

        Assertions.assertEquals(2, elements.size());
        Assertions.assertLinesMatch(
            List.of("Username is required.", "Password is required."),
            elements.stream().map(WebElement::getText).collect(Collectors.toList()));
    }

    @Test
    void logsInExpectingError() {

        // fill in the form

        WebElement usernameField = driver.findElement(By.id("username"));
        usernameField.sendKeys("invalid-username");

        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("invalid-password");

        // login

        WebElement loginButton = driver.findElement(By.cssSelector("#login-form > div.v-card__actions > button"));
        loginButton.click();

        // wait for login error to be present

        Assertions.assertDoesNotThrow(() -> wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("snackbar"), "Invalid credentials.")));
    }
}
