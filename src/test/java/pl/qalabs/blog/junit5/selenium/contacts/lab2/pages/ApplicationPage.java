package pl.qalabs.blog.junit5.selenium.contacts.lab2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class ApplicationPage {

    protected final RemoteWebDriver driver;
    final WebDriverWait wait;

    ApplicationPage(RemoteWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 1);
        if (!isAt()) {
            throw new IllegalStateException("Not the page we expected to be on.");
        }
    }

    protected abstract boolean isAt();

    public LoginPage logout() {
        WebElement logoutButton = driver.findElement(By.cssSelector("#app-nav button:nth-child(1) > div.v-btn__content"));
        logoutButton.click();

        return new LoginPage(driver);
    }

    public Snackbar snackbar() {
        return new Snackbar(driver);
    }
}
