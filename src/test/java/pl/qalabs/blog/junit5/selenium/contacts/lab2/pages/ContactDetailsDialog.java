package pl.qalabs.blog.junit5.selenium.contacts.lab2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ContactDetailsDialog extends ApplicationPage {

    private final By contactDetailsDialogLocator = By.id("contact-form");
    private WebElement contactDetailsDialog;

    ContactDetailsDialog(RemoteWebDriver driver) {
        super(driver);
        this.contactDetailsDialog = driver.findElement(contactDetailsDialogLocator);
    }

    @Override
    protected boolean isAt() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(contactDetailsDialogLocator));
        return true;
    }

    public Contact getContact() {
        return null;
    }
}
