package pl.qalabs.blog.junit5.selenium.contacts.lab2.tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.remote.RemoteWebDriver;

class ContactsAppTests {

    private RemoteWebDriver driver;

    @BeforeEach
    void setUp() {

    }


    @AfterEach
    void tearDown() {

    }

    @Test
    void createsBasicContact() {

    }

    @Test
    void createsContact() {

    }

    @Test
    void showsContactDetails() {

    }

    @Test
    void modifiesContactName() {

    }

    @Test
    void removesContact() {

    }

    @Test
    void searchesByName() {

    }

    @Test
    void searchesByLabel() {

    }
}
