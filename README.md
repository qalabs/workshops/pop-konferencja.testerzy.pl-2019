# JUnit5 Selenium Contacts App Automation Example

- Basic JUnit 5 Gradle Selenium Template
- Requires Java 11 to be installed. Change `sourceCompatibility` property in `build.gradle` to change supported Java version

## Setting up the environment

- https://blog.qalabs.pl/narzedzia/git-cmder/
- https://blog.qalabs.pl/narzedzia/selenium-przegladarki/
- https://blog.qalabs.pl/java/przygotowanie-srodowiska/

## Running the tests

    gradlew clean test
